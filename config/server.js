//constante que armazena o valor da porta
const port = 3000

/* declaração de depedências 
 * body-parser : interpretação body(corpo) da requisição
 * express : framework web
 */
const bodyParser = require('body-parser')
const express = require('express')

// instância do express
const server = express()

/* todas as requisições que chegarem no servidor
 * irão passar em sequência pelos middlewares
 * 'bodyParser.urlencoded' : faz o parse das requisições via formulário
 * 'bodyParser.json' : faz o parse das requisições via json
 */
server.use(bodyParser.urlencoded({extended : true}))
server.use(bodyParser.json())

server.listen(port, function () {

	console.log(`The backend server is runnig in port ${port}.`)

})

// o módulo fica disponível apenas no seu escopo
// para usá-lo em outros módulos precisamos fazer o exports
module.exports = server


