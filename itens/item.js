const restful = require('node-restful')
const mongoose = restful.mongoose

const itemSchema = new mongoose.Schema({
    name: { type: String, required: true },
    type: { type: String, required: true },
    amount: { type: Number, min: 0, required: true }
})

module.exports = restful.model('Item', itemSchema)

/*
 *  mongoose.Schema : mapeia para uma coleção do MongoDB e define a forma dos documentos
 *  dentro dessa coleção
 *  restful.model retorna um modelo do Mongoose, para que você possa interagir, isto é: 
 *  new Resource(), Resource.findById, etc.
 *  
 */