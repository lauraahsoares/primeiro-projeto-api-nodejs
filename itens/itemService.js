// importação do Schema item.js
const Item = require('./item')

// criando os métodos da API REST relacionados ao
// Schema "itemSchema" do MongoDB
Item.methods(['get', 'post', 'put', 'delete'])

//atualizando resposta dos métodos POST/PUT da API
Item.updateOptions({new : true, runValidators : true})


//exportando schema "item.js" com métodos adicionados
module.exports = Item


